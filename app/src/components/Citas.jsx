/*
 * File: Citas.jsx
 *
 * Created: 12 jun 2018 13:51:10
 * Last Modified: 09 jul 2018 12:08:21
 *
 * Copyright (c) 2018, Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 * Author: Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/.
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import { Grid, Row, Col } from 'react-bootstrap';
import { BeatLoader } from 'react-spinners';
import moment from 'moment-timezone';
import toastr from 'toastr';
import axios from 'axios';
import _ from 'lodash';

export default class Citas extends Component {
  static propTypes =  {
    beatLoaderColor: PropTypes.string.isRequired,
    beatLoaderSize: PropTypes.number.isRequired,
    language: PropTypes.string.isRequired,
    selectRowBgColor: PropTypes.string.isRequired,
    serverURL: PropTypes.string.isRequired
  };

  state = {
    citas: [],
    error: null,
    loading: true,
    updateSuccessful: null
  };

  onCellEdit = (row, fieldName, value) => {
    const { serverURL } = this.props;
    const register = Object.assign({}, row);

    register[fieldName] = value;

    axios.put(`${serverURL}/citas/${row.id}`, register)
      .then(response => {
        if (response.status === 204) {
          const { citas } = this.state;
          const cita = _.find(citas, { 'id': row.id });

          if (cita !== undefined) {
            const index = _.findIndex(citas, { 'id': row.id });

            cita[fieldName] = value;
            citas[index] = cita;

            this.setState({
              citas,
              updateSuccessful: true
            });
          }
        }
      })
      .catch(error => {
        this.setState({
          error,
          updateSuccessful: false
        });
      });
  }

  onDeleteRow = (id) => {
    const { serverURL } = this.props;

    axios.delete(`${serverURL}/citas/${id}`)
      .then(response => {
        if (response.status === 204) {
          let { citas } = this.state;

          citas = citas.filter((cita) => {
            return cita.id !== id[0];
          });

          this.setState({
            citas,
            updateSuccessful: true
          });
        }
      })
      .catch(error => {
        this.setState({
          error,
          updateSuccessful: false
        });
      });
  }

  onGetCitas = () => {
    const { serverURL } = this.props;

    axios.get(`${serverURL}/agendas`)
      .then(response => {
        this.setState({
          citas: response.data,
          error: false,
          loading: false
        });
      })
      .catch(error => {
        this.setState({
          error
        });
      });
  }

  componentDidMount() {
    this.onGetCitas();
  }

  render() {
    const { error, loading, updateSuccessful } = this.state;
    const { beatLoaderColor, beatLoaderSize, language,
      selectRowBgColor } = this.props;

    if (error) {
      toastr.error(error.message);
    }
    else if (updateSuccessful) {
      toastr.success('La transacción se realizó exitosamente.');
    }

    return(
      <React.Fragment>
        <Grid>
          <Row>
            <Col xs={ 2 } xsOffset={ 5 }>
              <BeatLoader color={ beatLoaderColor } loading={ loading }
                size={ beatLoaderSize } />
            </Col>
          </Row>
          <Row>
            <Col xs={ 12 }>
              <h1>Citas</h1>
            </Col>
          </Row>
          <Row>
            <Col xs={ 12 }>
              <CitasTable
                language={ language }
                onCellEdit={ this.onCellEdit }
                onDeleteRow={ this.onDeleteRow }
                selectRowBgColor={ selectRowBgColor }
                { ...this.state }
              />
            </Col>
          </Row>
        </Grid>
      </React.Fragment>
    );
  }
}

class CitasTable extends Component {
  static propTypes = {
    citas: PropTypes.array.isRequired,
    language: PropTypes.string.isRequired,
    onCellEdit: PropTypes.func.isRequired,
    onDeleteRow: PropTypes.func.isRequired,
    selectRowBgColor: PropTypes.string.isRequired
  };

  state = {
    cita: [ { id: 0, value: 'Revisión' }, { id: 1, value: 'Consulta' } ],
    confirmada: [ { id: 0, value: 'Sin confirmar' }, { id: 1, value: 'Confirmada' } ]
  }

  cellEditProp = {
    blurToSave: true,
    mode: 'dbclick'
  };

  selectRowProp = {
    bgColor: this.props.selectRowBgColor,
    mode: 'radio'
  };

  tableOptions = {
    deleteText: 'Eliminar',
    exportCSVText: 'Exportar a Excel',
    handleConfirmDeleteRow: this.createCustomConfirmDeleteRow,
    hidePageListOnlyOnePage: true,
    noDataText: 'No existe información para mostrar.',
    onCellEdit: this.props.onCellEdit,
    onDeleteRow: this.props.onDeleteRow
  };

  citaCVSFormatter = (cell) => {
    return cell === true || cell === 1 ? 'Consulta' : 'Revisión';
  }

  confirmadaCVSFormatter = (cell) => {
    return cell === true || cell === 1 ? 'Confirmada' : 'Sin confirmar';
  }

  fechaCVSFormatter = (cell) => {
    return moment(cell.split('T')[0]).format('DD-MM-YYYY');
  }

  horaCVSFormatter = (cell) => {
    return cell.substr(0, 5);
  }

  handlerClickCleanFiltered = () => {
    this.refs.cita.cleanFiltered();
    this.refs.confirmada.cleanFiltered();
    this.refs.fecha.cleanFiltered();
    this.refs.hora.cleanFiltered();
    this.refs.medico.cleanFiltered();
    this.refs.nombre.cleanFiltered();
    this.refs.observaciones.cleanFiltered();
  }

  observacionesCVSFormatter = (cell) => {
    return cell === null ? '' : cell;
  }

  usuarioCVSFormatter = (cell) => {
    return cell === null || cell === undefined ? '' : cell;
  }

  citaFormatter(cell) {
    return cell === true || cell === 1 ? 'Consulta' : 'Revisión';
  }

  confirmadaFormatter(cell) {
    return cell === true || cell === 1 ? 'Confirmada' : 'Sin confirmar';
  }

  createCustomConfirmDeleteRow(next) {
    if (confirm('¿Desea confirmar la eliminación del registro?')) {
      next();
    }
  }

  fechaFormatter(cell) {
    return moment(cell.split('T')[0]).format('DD-MM-YYYY');
  }

  horaFormatter(cell) {
    return cell.substr(0, 5);
  }

  render() {
    const { language } = this.props;

    moment.locale(language);

    return (
      <BootstrapTable
        cellEdit={ this.cellEditProp }
        data={ this.props.citas }
        deleteRow={ true }
        exportCSV={ true }
        hover={ true }
        options= { this.tableOptions }
        pagination={ true }
        selectRow={ this.selectRowProp }
        striped={ true }
      >
        <TableHeaderColumn
          dataField="id"
          hidden={ true }
          isKey={ true }
          width='7%'
        >
          Id
        </TableHeaderColumn>
        <TableHeaderColumn
          dataField="medico"
          editable={ false }
          filter={ { type: 'TextFilter',  placeholder: 'Médico', delay: 500 } }
          ref="medico"
          tdStyle={ { whiteSpace: 'pre-line' } }
        >
          Médico |&nbsp;
          <a className="highlight-title" onClick={ this.handlerClickCleanFiltered }
            style={ { cursor: 'pointer' } } title="Limpiar" >
            <i className="fa fa-times"></i>
          </a>
        </TableHeaderColumn>
        <TableHeaderColumn
          csvFormat={ this.horaCVSFormatter }
          dataField="hora"
          dataFormat={ this.horaFormatter }
          editable={ false }
          filter={ { type: 'TextFilter',  placeholder: 'Hora', delay: 500 } }
          filterFormatted={ true }
          ref="hora"
          width='7%'
        >
          Hora
        </TableHeaderColumn>
        <TableHeaderColumn
          csvFormat={ this.fechaCVSFormatter }
          dataField="fecha"
          dataFormat={ this.fechaFormatter }
          filter={ { type: 'TextFilter',  placeholder: 'Fecha', delay: 500 } }
          filterFormatted={ true }
          editable={ false }
          ref="fecha"
          width='10%'
        >
          Fecha
        </TableHeaderColumn>
        <TableHeaderColumn
          dataField="nombre"
          editable={ false }
          filter={ { type: 'TextFilter',  placeholder: 'Nombre', delay: 500 } }
          ref="nombre"
          tdStyle={ { whiteSpace: 'pre-line' } }
        >
          Nombre
        </TableHeaderColumn>
        <TableHeaderColumn
          className="th-editable-column"
          csvFormat={ this.citaCVSFormatter }
          dataField="consulta"
          dataFormat={ this.citaFormatter }
          editable={ { type: 'select', options: { values: this.state.cita, textKey: 'value', valueKey: 'id' } } }
          filter={ { type: 'TextFilter',  placeholder: 'Cita', delay: 500 } }
          filterFormatted={ true }
          ref="cita"
          width='10%'
        >
          Cita
        </TableHeaderColumn>
        <TableHeaderColumn
          dataField="domicilio"
          editable={ false }
          tdStyle={ { whiteSpace: 'pre-line' } }
        >
          Domicilio
        </TableHeaderColumn>
        <TableHeaderColumn
          dataField="telefono"
          editable={ false }
          tdStyle={ { whiteSpace: 'pre-line' } }
          width='10%'
        >
          Teléfono
        </TableHeaderColumn>
        <TableHeaderColumn
          className="th-editable-column"
          csvFormat={ this.confirmadaCVSFormatter }
          dataField="confirmada"
          dataFormat={ this.confirmadaFormatter }
          editable={ { type: 'select', options: { values: this.state.confirmada, textKey: 'value', valueKey: 'id' } } }
          filter={ { type: 'TextFilter',  placeholder: 'Confirmada', delay: 500 } }
          filterFormatted={ true }
          ref="confirmada"
          width='12%'
        >
          Confirmada
        </TableHeaderColumn>
        <TableHeaderColumn
          csvFormat={ this.observacionesCVSFormatter }
          dataField="observaciones"
          editable={ false }
          filter={ { type: 'TextFilter',  placeholder: 'Observaciones', delay: 500 } }
          ref="observaciones"
          tdStyle={ { whiteSpace: 'pre-line' } }
        >
          Observaciones
        </TableHeaderColumn>
        <TableHeaderColumn
          csvFormat={ this.usuarioCVSFormatter }
          dataField="usuario"
          editable={ false }
          width='12%'
        >
          Usuario
        </TableHeaderColumn>
      </BootstrapTable>
    );
  }
}
