/*
 * File: Pacientes.jsx
 *
 * Created: 01 jun 2018 21:38:30
 * Last Modified: 25 jul 2018 08:33:37
 *
 * Copyright (c) 2018, Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 * Author: Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/.
 */

import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {
  BootstrapTable,
  TableHeaderColumn,
  InsertModalHeader,
  InsertModalFooter,
} from 'react-bootstrap-table';
import {Grid, Row, Col} from 'react-bootstrap';
import {BeatLoader} from 'react-spinners';
import toastr from 'toastr';
import axios from 'axios';
import _ from 'lodash';

export default class Pacientes extends Component {
  static propTypes = {
    beatLoaderColor: PropTypes.string.isRequired,
    beatLoaderSize: PropTypes.number.isRequired,
    selectRowBgColor: PropTypes.string.isRequired,
    serverURL: PropTypes.string.isRequired,
  };

  state = {
    error: null,
    loading: true,
    pacientes: [],
    updateSuccessful: null,
  };

  onAddRow = row => {
    const {serverURL} = this.props;

    axios
      .post(`${serverURL}/pacientes`, row)
      .then(response => {
        const {pacientes} = this.state;

        pacientes.push(response.data);

        this.setState({
          pacientes,
          updateSuccessful: true,
        });
      })
      .catch(error => {
        this.setState({
          error,
          updateSuccessful: false,
        });
      });
  };

  onCellEdit = (row, fieldName, value) => {
    const {serverURL} = this.props;
    const register = Object.assign({}, row);

    register[fieldName] = value;

    axios
      .put(`${serverURL}/pacientes/${row.id}`, register)
      .then(response => {
        if (response.status === 204) {
          const {pacientes} = this.state;
          const paciente = _.find(pacientes, {id: row.id});

          if (paciente !== undefined) {
            const index = _.findIndex(pacientes, {id: row.id});

            paciente[fieldName] = value;
            pacientes[index] = paciente;

            this.setState({
              pacientes,
              updateSuccessful: true,
            });
          }
        }
      })
      .catch(error => {
        this.setState({
          updateSuccessful: false,
          error: error,
        });
      });
  };

  onDeleteRow = id => {
    const {serverURL} = this.props;

    axios
      .delete(`${serverURL}/pacientes/${id}`)
      .then(response => {
        if (response.status === 204) {
          let {pacientes} = this.state;

          pacientes = pacientes.filter(paciente => {
            return paciente.id !== id[0];
          });

          this.setState({
            pacientes,
            updateSuccessful: true,
          });
        }
      })
      .catch(error => {
        this.setState({
          error,
          updateSuccessful: false,
        });
      });
  };

  onGetPacientes = () => {
    const {serverURL} = this.props;

    axios
      .get(`${serverURL}/pacientes`)
      .then(response => {
        this.setState({
          error: false,
          loading: false,
          pacientes: response.data,
        });
      })
      .catch(error => {
        this.setState({
          error,
        });
      });
  };

  componentDidMount() {
    this.onGetPacientes();
  }

  render() {
    const {error, loading, updateSuccessful} = this.state;
    const {beatLoaderColor, beatLoaderSize, selectRowBgColor} = this.props;

    if (error) {
      toastr.error(error.message);
    } else if (updateSuccessful) {
      toastr.success('La transacción se realizó exitosamente.');
    }

    return (
      <React.Fragment>
        <Grid>
          <Row>
            <Col xs={2} xsOffset={5}>
              <BeatLoader
                color={beatLoaderColor}
                loading={loading}
                size={beatLoaderSize}
              />
            </Col>
          </Row>
          <Row>
            <Col xs={12}>
              <h1>Pacientes</h1>
            </Col>
          </Row>
          <Row>
            <Col xs={12}>
              <PacientesTable
                onAddRow={this.onAddRow}
                onCellEdit={this.onCellEdit}
                onDeleteRow={this.onDeleteRow}
                selectRowBgColor={selectRowBgColor}
                {...this.state}
              />
            </Col>
          </Row>
        </Grid>
      </React.Fragment>
    );
  }
}

class PacientesTable extends Component {
  static propTypes = {
    pacientes: PropTypes.array.isRequired,
    onAddRow: PropTypes.func.isRequired,
    onCellEdit: PropTypes.func.isRequired,
    onDeleteRow: PropTypes.func.isRequired,
    selectRowBgColor: PropTypes.string.isRequired,
  };

  cellEditProp = {
    blurToSave: true,
    mode: 'dbclick',
  };

  selectRowProp = {
    bgColor: this.props.selectRowBgColor,
    mode: 'radio',
  };

  tableOptions = {
    clearSearch: true,
    clearSearchBtn: this.createCustomClearButton,
    deleteText: 'Eliminar',
    exportCSVText: 'Exportar a Excel',
    handleConfirmDeleteRow: this.createCustomConfirmDeleteRow,
    hidePageListOnlyOnePage: true,
    insertModalHeader: this.createCustomModalHeader,
    insertModalFooter: this.createCustomModalFooter,
    insertText: 'Nuevo',
    noDataText: 'No existe información para mostrar.',
    onAddRow: this.props.onAddRow,
    onCellEdit: this.props.onCellEdit,
    onDeleteRow: this.props.onDeleteRow,
  };

  handleDataValidator(value) {
    const response = {
      isValid: true,
      notification: {type: 'success', msg: '', title: ''},
    };

    if (!value) {
      response.isValid = false;
      response.notification.type = 'error';
      response.notification.msg = 'Este campo debe ser completado.';
      response.notification.title = 'Información requerida.';
    }

    return response;
  }

  createCustomModalHeader() {
    return <InsertModalHeader title="Agregar paciente" />;
  }

  createCustomModalFooter() {
    return <InsertModalFooter saveBtnText="Guardar" closeBtnText="Cerrar" />;
  }

  createCustomClearButton(onClick) {
    return (
      <button className="btn btn-default" onClick={onClick}>
        Limpiar
      </button>
    );
  }

  createCustomConfirmDeleteRow(next) {
    if (confirm('¿Desea confirmar la eliminación del registro?')) {
      next();
    }
  }

  render() {
    return (
      <BootstrapTable
        cellEdit={this.cellEditProp}
        data={this.props.pacientes}
        deleteRow={true}
        exportCSV={true}
        hover={true}
        insertRow={true}
        options={this.tableOptions}
        pagination={true}
        search={true}
        searchPlaceholder="Buscar"
        selectRow={this.selectRowProp}
        striped={true}>
        <TableHeaderColumn
          autoValue={true}
          dataField="id"
          dataSort={true}
          hidden={true}
          isKey={true}
          width="5%">
          Id
        </TableHeaderColumn>
        <TableHeaderColumn
          dataField="nombre"
          dataSort={true}
          editable={{validator: this.handleDataValidator}}
          tdStyle={{whiteSpace: 'pre-line'}}
          searchable={true}>
          Nombre
        </TableHeaderColumn>
        <TableHeaderColumn
          dataField="domicilio"
          editable={{type: 'textarea', validator: this.handleDataValidator}}
          tdStyle={{whiteSpace: 'pre-line'}}>
          Domicilio
        </TableHeaderColumn>
        <TableHeaderColumn
          dataField="telefono"
          editable={{validator: this.handleDataValidator}}
          width="10%">
          Teléfono
        </TableHeaderColumn>
        <TableHeaderColumn
          dataField="observaciones"
          editable={{type: 'textarea'}}
          tdStyle={{whiteSpace: 'pre-line'}}>
          Observaciones
        </TableHeaderColumn>
      </BootstrapTable>
    );
  }
}
