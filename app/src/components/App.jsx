/*
 * File: App.jsx
 *
 * Created: 27 may 2018 22:47:33
 * Last Modified: 07 ago 2018 19:14:18
 *
 * Copyright (c) 2018, Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 * Author: Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/.
 */

import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {HashRouter, Route, Switch, Link} from 'react-router-dom';
import {
  FormGroup,
  FormControl,
  Button,
  Navbar,
  Nav,
  NavItem,
} from 'react-bootstrap';
import {LinkContainer} from 'react-router-bootstrap';
import Especialidades from './Especialidades';
import Medicos from './Medicos';
import Pacientes from './Pacientes';
import Agenda from './Agenda';
import Citas from './Citas';
import Inicio from './Inicio';
import Usuarios from './Usuarios';
import Etiquetas from './Etiquetas';
import MedicalMeetLogo from '../assets/medicalmeet.png';
import Patron from '../assets/patron.png';
import toastr from 'toastr';
import axios from 'axios';

export default class App extends Component {
  state = {
    beatLoaderColor: '#c4c4c4',
    beatLoaderSize: 36,
    contrasena: null,
    error: null,
    expandRowBgColor: '#dedede',
    language: 'es',
    passport: [],
    selectRowBgColor: '#f0ca4d',
    serverURL: 'http://192.168.1.200:3000',
    signIn: null,
    timeZone: 'America/Mexico_City',
    usuario: null,
  };

  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value,
      error: null,
    });
  };

  onLogin = async () => {
    const {contrasena, usuario} = this.state;

    await this.onGetLogin(usuario);

    const {error, passport} = this.state;

    if (!error) {
      if (passport.contrasena === contrasena) {
        this.setState({
          signIn: true,
        });
      } else {
        this.setState({
          error: true,
        });
      }
    }
  };

  onGetLogin = async usuario => {
    const {serverURL} = this.state;

    await axios
      .get(`${serverURL}/login/${usuario}`)
      .then(response => {
        this.setState({
          error: false,
          passport: response.data,
        });
      })
      .catch(error => {
        this.setState({
          error,
        });
      });
  };

  render() {
    const {error, signIn} = this.state;
    let element;

    if (error) {
      toastr.error('No existe el usuario o contraseña.');
    }

    if (!signIn) {
      element = (
        <Login handleChange={this.handleChange} onLogin={this.onLogin} />
      );
    } else {
      element = (
        <HashRouter>
          <Toolbar {...this.state} />
        </HashRouter>
      );
    }

    return element;
  }
}

class Login extends Component {
  static propTypes = {
    handleChange: PropTypes.func.isRequired,
    onLogin: PropTypes.func.isRequired,
  };

  render() {
    return (
      <React.Fragment>
        <form className="text-center form-signin">
          <img
            className="mb-4 patron-branding"
            src={Patron}
            title="Centro de Especialidades de Huajuapan"
          />
          <h1 className="h3 mb-3 font-weight-normal">Iniciar sesión</h1>
          <FormGroup>
            <FormControl
              id="usuario"
              onChange={this.props.handleChange}
              type="text"
              placeholder="Usuario"
            />
            <FormControl
              id="contrasena"
              onChange={this.props.handleChange}
              type="password"
              placeholder="Contraseña"
            />
          </FormGroup>
          <Button
            block
            bsSize="lg"
            bsStyle="primary"
            onClick={this.props.onLogin}>
            Acceder
          </Button>
          <p className="mt-5 mb-3 text-muted">
            &copy;{' '}
            <a href="mailto:mario.i.ruvalcaba@gmail.com">Iván Ruvalcaba</a> —
            2018.
          </p>
        </form>
      </React.Fragment>
    );
  }
}

class Toolbar extends Component {
  static propTypes = {
    passport: PropTypes.object.isRequired,
  };

  render() {
    const {passport} = this.props;
    const disableMenu = passport.tipo_usuario !== 1 ? true : false;

    return (
      <React.Fragment>
        <Navbar fixedTop={true} inverse={true}>
          <Navbar.Header>
            <Navbar.Brand>
              <Link to="/">
                <img
                  className="app-branding"
                  src={MedicalMeetLogo}
                  title="MedicalMeet"
                />
              </Link>
            </Navbar.Brand>
          </Navbar.Header>
          <Nav>
            <LinkContainer to="/">
              <NavItem eventKey={1} href="/">
                Inicio
              </NavItem>
            </LinkContainer>
            <LinkContainer to="/agenda">
              <NavItem eventKey={2} href="/agenda">
                Agenda
              </NavItem>
            </LinkContainer>
            <LinkContainer to="/citas">
              <NavItem eventKey={3} href="/citas">
                Citas
              </NavItem>
            </LinkContainer>
            <LinkContainer to="/pacientes">
              <NavItem eventKey={4} href="/pacientes">
                Pacientes
              </NavItem>
            </LinkContainer>
            <LinkContainer to="/medicos">
              <NavItem eventKey={5} href="/medicos">
                Médicos
              </NavItem>
            </LinkContainer>
            <LinkContainer to="/especialidades">
              <NavItem eventKey={6} href="/especialidades">
                Especialidades
              </NavItem>
            </LinkContainer>
            <LinkContainer to="/etiquetas">
              <NavItem eventKey={7} href="/etiquetas">
                Etiquetas
              </NavItem>
            </LinkContainer>
            <LinkContainer to="/usuarios">
              <NavItem disabled={disableMenu} eventKey={8} href="/usuarios">
                Usuarios
              </NavItem>
            </LinkContainer>
          </Nav>
        </Navbar>
        <Switch>
          <Route
            exact={true}
            path="/"
            render={() => <Inicio {...this.props} />}
          />
          <Route path="/agenda" render={() => <Agenda {...this.props} />} />
          <Route path="/citas" render={() => <Citas {...this.props} />} />
          <Route
            path="/pacientes"
            render={() => <Pacientes {...this.props} />}
          />
          <Route path="/medicos" render={() => <Medicos {...this.props} />} />
          <Route
            path="/especialidades"
            render={() => <Especialidades {...this.props} />}
          />
          <Route
            path="/etiquetas"
            render={() => <Etiquetas {...this.props} />}
          />
          <Route path="/usuarios" render={() => <Usuarios {...this.props} />} />
        </Switch>
      </React.Fragment>
    );
  }
}
