/*
 * File: Medicos.jsx
 *
 * Created: 30 may 2018 09:48:14
 * Last Modified: 04 jul 2018 13:24:55
 *
 * Copyright (c) 2018, Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 * Author: Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/.
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { BootstrapTable, TableHeaderColumn, InsertModalHeader,
  InsertModalFooter } from 'react-bootstrap-table';
import { Grid, Row, Col, Alert } from 'react-bootstrap';
import { BeatLoader } from 'react-spinners';
import toastr from 'toastr';
import axios from 'axios';
import _ from 'lodash';

export default class Medicos extends Component {
  static propTypes =  {
    beatLoaderColor: PropTypes.string.isRequired,
    beatLoaderSize: PropTypes.number.isRequired,
    selectRowBgColor: PropTypes.string.isRequired,
    serverURL: PropTypes.string.isRequired,
    timeZone: PropTypes.string.isRequired
  };

  state = {
    error: null,
    especialidades: [],
    loading: true,
    isEspecialidadesEmpty: false,
    medicos: [],
    updateSuccessful: null
  };

  onAddRow = (row) => {
    const { serverURL } = this.props;

    axios.post(`${serverURL}/medicos`, row)
      .then(response => {
        const { medicos } = this.state;

        medicos.push(response.data);

        this.setState({
          medicos,
          updateSuccessful: true
        });
      })
      .catch(error => {
        this.setState({
          error,
          updateSuccessful: false
        });
      });
  }

  onCellEdit = (row, fieldName, value) => {
    const { serverURL } = this.props;
    const register = Object.assign({}, row);

    register[fieldName] = value;

    axios.put(`${serverURL}/medicos/${row.id}`, register)
      .then(response => {
        if (response.status === 204) {
          const { medicos } = this.state;
          const medico = _.find(medicos, { 'id': row.id });

          if (medico !== undefined) {
            const index = _.findIndex(medicos, { 'id': row.id });

            medico[fieldName] = value;
            medicos[index] = medico;

            this.setState({
              medicos,
              updateSuccessful: true
            });
          }
        }
      })
      .catch(error => {
        this.setState({
          error,
          updateSuccessful: false
        });
      });
  }

  onDeleteRow = (id) => {
    const { serverURL } = this.props;

    axios.delete(`${serverURL}/medicos/${id}`)
      .then(response => {
        if (response.status === 204) {
          let { medicos } = this.state;

          medicos = medicos.filter((medico) => {
            return medico.id !== id[0];
          });

          this.setState({
            medicos,
            updateSuccessful: true
          });
        }
      })
      .catch(error => {
        this.setState({
          error,
          updateSuccessful: false
        });
      });
  }

  onGetEspecialidades = () => {
    const { serverURL } = this.props;

    axios.get(`${serverURL}/especialidades`)
      .then(response => {
        if (response.data.length > 0) {
          const especialidades = _.map(response.data, especialidad =>
            _.omit(especialidad, ['descripcion', 'created_at', 'updated_at']
            ));

          this.setState({
            error: false,
            especialidades,
            loading: false
          });
        }
        else {
          this.setState({
            isEspecialidadesEmpty: true
          });
        }
      })
      .catch(error => {
        this.setState({
          error
        });
      });
  }

  onGetMedicos = () => {
    const { serverURL } = this.props;

    axios.get(`${serverURL}/medicos`)
      .then(response => {
        this.setState({
          medicos: response.data
        });
        this.onGetEspecialidades();
      })
      .catch(error => {
        this.setState({
          error
        });
      });
  }

  componentDidMount() {
    this.onGetMedicos();
  }

  render() {
    const { error, loading, updateSuccessful,
      isEspecialidadesEmpty } = this.state;
    const { beatLoaderColor, beatLoaderSize, selectRowBgColor } = this.props;
    let element;

    if (error) {
      toastr.error(error.message);
    }
    else if (updateSuccessful) {
      toastr.success('La transacción se realizó exitosamente.');
    }

    if (isEspecialidadesEmpty) {
      element = (
        <React.Fragment>
          <Grid>
            <Row>
              <Col xs={ 12 }>
                <h1>Médicos</h1>
                <Alert bsStyle="info">
                  Asegurese de que exista al menos una especialidad antes&nbsp;
                  de intentar agregar un nuevo médico.
                </Alert>
              </Col>
            </Row>
          </Grid>
        </React.Fragment>
      );
    }
    else {
      element = (
        <React.Fragment>
          <Grid>
            <Row>
              <Col xs={ 2 } xsOffset={ 5 }>
                <BeatLoader color={ beatLoaderColor } loading={ loading }
                  size={ beatLoaderSize } />
              </Col>
            </Row>
            <Row>
              <Col xs={ 12 }>
                <h1>Médicos</h1>
              </Col>
            </Row>
            <Row>
              <Col xs={ 12 }>
                <MedicosTable
                  onAddRow={ this.onAddRow }
                  onCellEdit={ this.onCellEdit }
                  onDeleteRow={ this.onDeleteRow }
                  selectRowBgColor={ selectRowBgColor }
                  { ...this.state }
                />
              </Col>
            </Row>
          </Grid>
        </React.Fragment>
      );
    }

    return (element);
  }
}

class MedicosTable extends Component {
  static propTypes = {
    especialidades: PropTypes.array.isRequired,
    medicos: PropTypes.array.isRequired,
    onAddRow: PropTypes.func.isRequired,
    onCellEdit: PropTypes.func.isRequired,
    onDeleteRow: PropTypes.func.isRequired,
    selectRowBgColor: PropTypes.string.isRequired
  }

  state = {
    duracionConsulta: [5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60]
  }

  cellEditProp = {
    mode: 'dbclick',
    blurToSave: true
  };

  selectRowProp = {
    bgColor: this.props.selectRowBgColor,
    mode: 'radio'
  };

  tableOptions = {
    clearSearch: true,
    clearSearchBtn: this.createCustomClearButton,
    deleteText: 'Eliminar',
    exportCSVText: 'Exportar a Excel',
    handleConfirmDeleteRow: this.createCustomConfirmDeleteRow,
    hidePageListOnlyOnePage: true,
    insertModalHeader: this.createCustomModalHeader,
    insertModalFooter: this.createCustomModalFooter,
    insertText: 'Nuevo',
    onAddRow: this.props.onAddRow,
    onCellEdit: this.props.onCellEdit,
    onDeleteRow: this.props.onDeleteRow,
    noDataText: 'No existe información para mostrar.'
  };

  especialidadFormatter = (cell) => {
    const { especialidades } = this.props;
    const especialidad = this.handleGainValue(especialidades,
      'especialidad', cell);

    return especialidad;
  }

  especialidadCVSFormatter = (cell) => {
    const { especialidades } = this.props;
    const especialidad = this.handleGainValue(especialidades,
      'especialidad', cell);

    return especialidad;
  }

  emailCVSFormatter(cell) {
    return cell === null ? '' : cell;
  }

  handleGainValue(collection, key, value) {
    const object = _.find(collection, { id: value });
    let returnedValue = '';

    if (_.has(object, key))
      returnedValue = object[key];

    return returnedValue;
  }

  handleDataValidator(value) {
    const response = { isValid: true,
      notification: { type: 'success', msg: '', title: '' } };

    if (!value) {
      response.isValid = false;
      response.notification.type = 'error';
      response.notification.msg = 'Este campo debe ser completado.';
      response.notification.title = 'Información requerida.';
    }

    return response;
  }

  handleProfessionalLicenceValidator(value) {
    const response = { isValid: true,
      notification: { type: 'success', msg: '', title: '' } };
    const alphanumericRegEx = /^[0-9a-zA-Z]+$/;

    if (!value.match(alphanumericRegEx)) {
      response.isValid = false;
      response.notification.type = 'error';
      response.notification.msg =
        'Este campo debe ser completado usando únicamente números y/o letras [0-9,a-z,A-Z].';
      response.notification.title = 'Información requerida.';
    }

    return response;
  }

  createCustomModalHeader() {
    return <InsertModalHeader title='Agregar médico' />;
  }

  createCustomModalFooter() {
    return <InsertModalFooter saveBtnText='Guardar' closeBtnText='Cerrar' />;
  }

  createCustomClearButton(onClick) {
    return <button className='btn btn-default' onClick={ onClick }>Limpiar</button>;
  }

  createCustomConfirmDeleteRow(next) {
    if (confirm('¿Desea confirmar la eliminación del registro?')) {
      next();
    }
  }

  render() {
    const { duracionConsulta } = this.state;

    return (
      <BootstrapTable
        data={ this.props.medicos }
        cellEdit={ this.cellEditProp }
        selectRow={ this.selectRowProp }
        options= { this.tableOptions }
        searchPlaceholder='Buscar'
        deleteRow={ true }
        exportCSV={ true }
        hover={ true }
        insertRow={ true }
        pagination={ true }
        search={ true }
        striped={ true }
      >
        <TableHeaderColumn
          dataField="id"
          csvFieldType="number"
          width='5%'
          autoValue={ true }
          hidden={ true }
          isKey={ true }
        >
          Id
        </TableHeaderColumn>
        <TableHeaderColumn
          dataField="nombre"
          editable={ { validator: this.handleDataValidator } }
          tdStyle={ { whiteSpace: 'pre-line' } }
          dataSort={ true }
          searchable={ true }
        >
          Nombre
        </TableHeaderColumn>
        <TableHeaderColumn
          dataField="especialidad_id"
          editable={ { type: 'select', options: { values: this.props.especialidades, textKey: 'especialidad', valueKey: 'id' } } }
          dataFormat={ this.especialidadFormatter }
          csvFormat={ this.especialidadCVSFormatter }
          dataSort={ true }
          tdStyle={ { whiteSpace: 'pre-line' } }
          width='12%'
        >
          Especialidad
        </TableHeaderColumn>
        <TableHeaderColumn
          dataField="cedula_profesional"
          editable={ { validator: this.handleProfessionalLicenceValidator } }
          width='8%'
        >
          Céd. Prof.
        </TableHeaderColumn>
        <TableHeaderColumn
          dataField="telefono"
          editable={ { validator: this.handleDataValidator } }
          searchable={ true }
          width='10%'
        >
          Teléfono
        </TableHeaderColumn>
        <TableHeaderColumn
          csvFormat={ this.emailCVSFormatter }
          dataField="email"
          width='15%'
        >
          Email
        </TableHeaderColumn>
        <TableHeaderColumn
          dataField="horario"
          editable={ { type: 'textarea', validator: this.handleDataValidator } }
          tdStyle={ { whiteSpace: 'pre-line' } }
        >
          Horario
        </TableHeaderColumn>
        <TableHeaderColumn
          dataField="duracion_consulta"
          editable={ { type: 'select', options: { values: duracionConsulta } } }
          width='9%'
        >
          Consulta (min.)
        </TableHeaderColumn>
        <TableHeaderColumn
          dataField="procedimientos"
          editable={ { type: 'textarea', validator: this.handleDataValidator } }
          tdStyle={ { whiteSpace: 'pre-line' } }
        >
          Procedimientos
        </TableHeaderColumn>
        <TableHeaderColumn
          dataField="observaciones"
          editable={ { type: 'textarea', validator: this.handleDataValidator } }
          tdStyle={ { whiteSpace: 'pre-line' } }
        >
          Observaciones
        </TableHeaderColumn>
      </BootstrapTable>
    );
  }
}
