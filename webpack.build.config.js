/*
 * File: webpack.build.config.js
 *
 * Created: 17 jul 2018 14:16:10
 * Last Modified: 17 jul 2018 19:36:27
 *
 * Copyright (c) 2018, Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 * Author: Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/.
 */

const webpack = require('webpack');
const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const MinifyPlugin = require('babel-minify-webpack-plugin');

module.exports = {
  entry: './app/src/renderer.js',
  output: {
    path: __dirname + '/app/build',
    publicPath: 'build/',
    filename: 'bundle.js',
  },
  target: 'electron-renderer',
  watch: true,
  module: {
    rules: [
      {
        test: /\.(js|jsx)?$/,
        exclude: /node_modules/,
        include: path.join(__dirname, 'app'),
        loader: 'babel-loader',
      },
      {
        test: /\.css$/,
        use: [MiniCssExtractPlugin.loader, 'css-loader'],
      },
      {
        test: /\.(eot|svg|ttf|woff|woff2)$/,
        use: [{loader: 'file-loader?name=font/[name]__[hash:base64:5].[ext]'}],
      },
      {
        test: /\.(png|jpe?g|gif|svg)$/,
        use: [{loader: 'file-loader?name=img/[name]__[hash:base64:5].[ext]'}],
      },
    ],
  },
  plugins: [
    new MiniCssExtractPlugin({filename: 'bundle.css'}),
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
    }),
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify('production'),
    }),
    new MinifyPlugin(),
  ],
  resolve: {
    extensions: ['.js', '.json', '.jsx'],
  },
};
