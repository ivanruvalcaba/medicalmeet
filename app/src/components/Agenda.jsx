/*
 * File: Agenda.jsx
 *
 * Created: 02 jun 2018 08:43:33
 * Last Modified: 27 jul 2018 14:11:11
 *
 * Copyright (c) 2018, Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 * Author: Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/.
 */

import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {
  Grid,
  Row,
  Col,
  FormGroup,
  FormControl,
  ControlLabel,
  Radio,
  Button,
  ButtonToolbar,
  Modal,
  Alert,
  Well,
} from 'react-bootstrap';
import {BeatLoader} from 'react-spinners';
import {Typeahead} from 'react-bootstrap-typeahead';
import BigCalendar from 'react-big-calendar';
import momentLocalizer from 'react-widgets-moment';
import {DateTimePicker} from 'react-widgets';
import InputNumber from 'rc-input-number';
import moment from 'moment-timezone';
import toastr from 'toastr';
import axios from 'axios';
import _ from 'lodash';

export default class Agenda extends Component {
  static propTypes = {
    beatLoaderColor: PropTypes.string.isRequired,
    beatLoaderSize: PropTypes.number.isRequired,
    language: PropTypes.string.isRequired,
    serverURL: PropTypes.string.isRequired,
    timeZone: PropTypes.string.isRequired,
  };

  state = {
    citas: [],
    duracionConsulta: null,
    error: null,
    loading: true,
    isMedicosEmpty: false,
    isPacientesEmpty: false,
    medico: {},
    medicos: [],
    pacientes: [],
    passport: [],
    setCalendarDay: new Date(moment.tz(Date.now(), this.props.timeZone)),
    setCalendarView: 'month',
    updateSuccessful: null,
  };

  onGetLogin = async usuario => {
    const {serverURL} = this.props;

    this.setState({
      errorSignIn: false,
      updateSuccessful: false,
    });

    await axios
      .get(`${serverURL}/login/${usuario}`)
      .then(response => {
        this.setState({
          error: false,
          passport: response.data,
        });
      })
      .catch(error => {
        this.setState({
          error,
          updateSuccessful: false,
        });
      });
  };

  onGetMedicos = () => {
    const {serverURL} = this.props;

    axios
      .get(`${serverURL}/medicos`)
      .then(response => {
        if (response.data.length > 0) {
          const medicos = _.map(response.data, medico =>
            _.omit(medico, [
              'telefono',
              'email',
              'horario',
              'observaciones',
              'especialidad_id',
              'cedula_profesional',
              'procedimientos',
              'created_at',
              'updated_at',
            ]),
          );

          this.setState({
            error: false,
            loading: false,
            medicos,
          });
        } else {
          this.setState({
            isMedicosEmpty: true,
            loading: false,
          });
        }
      })
      .catch(error => {
        this.setState({
          error,
        });
      });
  };

  onGetPacientes = () => {
    const {serverURL} = this.props;

    axios
      .get(`${serverURL}/pacientes`)
      .then(response => {
        if (response.data.length > 0) {
          const pacientes = _.map(response.data, paciente =>
            _.omit(paciente, [
              'foraneo',
              'email',
              'alergias',
              'observaciones',
              'created_at',
              'updated_at',
            ]),
          );

          this.setState({
            error: false,
            loading: false,
            pacientes,
          });
        } else {
          this.setState({
            isPacientesEmpty: true,
            loading: false,
          });
        }
      })
      .catch(error => {
        this.setState({
          error,
        });
      });
  };

  onSetDuracionConsulta = duracionConsulta => {
    this.setState({
      duracionConsulta,
    });
  };

  componentDidMount() {
    this.onGetMedicos();
    this.onGetPacientes();
  }

  onGetAgenda(medico) {
    const {serverURL} = this.props;

    if (_.has(medico, 'id')) {
      axios
        .get(`${serverURL}/agenda/${medico.id}`)
        .then(response => {
          if (response.data.length > 0) {
            const agenda = _.map(response.data, cita =>
              _.omit(cita, ['medico_id', 'medico_nombre']),
            );
            const citas = agenda.map(cita => ({
              id: cita.id,
              start: new Date(
                moment(
                  new Date(cita.fecha).toISOString().split('T')[0] +
                    ' ' +
                    cita.hora,
                ),
              ),
              end: new Date(
                moment(
                  new Date(cita.fecha).toISOString().split('T')[0] +
                    ' ' +
                    cita.hora,
                ).add(cita.duracion, 'minutes'),
              ),
              title: cita.nombre,
            }));

            this.setState({
              citas: citas,
            });
          }
        })
        .catch(error => {
          this.setState({
            error: error,
          });
        });
    }

    this.setState({
      duracionConsulta: medico.duracion_consulta,
      medico,
      updateSuccessful: false,
    });
  }

  onAddCita = async (login, cita) => {
    await this.onGetLogin(login.usuario);

    const {passport} = this.state;

    if (passport.length !== 0) {
      const {serverURL} = this.props;
      const usuario = Object.assign({}, passport);

      if (login.contrasena === usuario.contrasena) {
        cita.usuario_id = usuario.id;

        axios
          .post(`${serverURL}/citas`, cita)
          .then(response => {
            this.setState({
              updateSuccessful: true,
            });
          })
          .catch(error => {
            this.setState({
              error,
              updateSuccessful: false,
            });
          });
      } else {
        this.setState({
          errorSignIn: true,
        });
      }
    }
  };

  render() {
    const {
      error,
      errorSignIn,
      setCalendarDay,
      setCalendarView,
      loading,
      isMedicosEmpty,
      isPacientesEmpty,
      citas,
      medicos,
      updateSuccessful,
    } = this.state;
    const {beatLoaderColor, beatLoaderSize, language, timeZone} = this.props;
    let element;

    moment.locale(language);
    BigCalendar.momentLocalizer(moment);
    momentLocalizer();

    if (error) {
      toastr.error(error.message);
    } else if (errorSignIn) {
      toastr.error('No existe el usuario o contraseña');
    } else if (updateSuccessful) {
      toastr.success('La transacción se realizó exitosamente.');
    }

    if (isMedicosEmpty || isPacientesEmpty) {
      element = (
        <React.Fragment>
          <Grid>
            <Row>
              <Col xs={12}>
                <h1>Agenda</h1>
                <Alert bsStyle="info">
                  Asegurese de que se haya dado de alta a un médico y un&nbsp;
                  un paciente antes de intentar agendar una cita.
                </Alert>
              </Col>
            </Row>
          </Grid>
        </React.Fragment>
      );
    } else {
      element = (
        <React.Fragment>
          <Grid>
            <Row>
              <Col xs={2} xsOffset={5}>
                <BeatLoader
                  color={beatLoaderColor}
                  loading={loading}
                  size={beatLoaderSize}
                />
              </Col>
            </Row>
            <Row>
              <Col xs={12}>
                <h1>Agenda</h1>
              </Col>
            </Row>
            <Row>
              <Col xs={8}>
                <Row>
                  <Col xs={4} xsOffset={1}>
                    <h2>Ir a fecha</h2>
                    <DateTimePicker
                      onChange={setCalendarDay =>
                        this.setState({
                          setCalendarDay,
                          setCalendarView: 'day',
                        })
                      }
                      placeholder="Seleccione una fecha."
                      time={false}
                    />
                  </Col>
                  <Col xs={6}>
                    <h2>Médico</h2>
                    <Typeahead
                      labelKey={option => `${option.id}, ${option.nombre}`}
                      onChange={medico => {
                        const selectedMedico = Object.assign({}, medico[0]);
                        this.onGetAgenda(selectedMedico);
                      }}
                      options={medicos}
                      placeholder="Nombre del médico."
                      ref={ref => (this._typeahead = ref)}
                      renderMenuItemChildren={option => (
                        <div>{option.nombre}</div>
                      )}
                    />
                    <ButtonToolbar style={{marginTop: '10px'}}>
                      <Button
                        onClick={() => this._typeahead.getInstance().clear()}>
                        Limpiar
                      </Button>
                    </ButtonToolbar>
                  </Col>
                </Row>
                <br />
                <br />
                <Row>
                  <Col xs={12}>
                    <BigCalendar
                      date={setCalendarDay}
                      events={citas}
                      messages={{
                        today: 'hoy',
                        previous: 'anterior',
                        next: 'siguiente',
                        day: 'día',
                        week: 'semanal',
                        month: 'mensual',
                        date: 'Fecha',
                        time: 'Hora',
                        event: 'Evento',
                        showMore: total => `+${total} más`,
                      }}
                      style={{height: '50vh'}}
                      view={setCalendarView}
                      views={{
                        day: true,
                        week: true,
                        month: true,
                      }}
                      onNavigate={date =>
                        this.setState({
                          setCalendarDay: date,
                        })
                      }
                      onSelectEvent={event =>
                        alert(
                          'Paciente: ' +
                            event.title +
                            '\n\n' +
                            'Inicia: ' +
                            event.start.toLocaleString() +
                            '\n\n' +
                            'Termina: ' +
                            event.end.toLocaleString(),
                        )
                      }
                      onView={setCalendarView =>
                        this.setState({
                          setCalendarView,
                        })
                      }
                      popup
                    />
                  </Col>
                </Row>
                <br />
                <br />
              </Col>
              <Col xs={4}>
                <h2>Agendar cita</h2>
                <Row>
                  <Col xs={12}>
                    <AgendaForm
                      timeZone={timeZone}
                      onAddCita={this.onAddCita}
                      onSetDuracionConsulta={this.onSetDuracionConsulta}
                      {...this.state}
                    />
                  </Col>
                </Row>
              </Col>
            </Row>
          </Grid>
        </React.Fragment>
      );
    }

    return element;
  }
}

class AgendaForm extends Component {
  static propTypes = {
    citas: PropTypes.array.isRequired,
    duracionConsulta: PropTypes.number,
    medico: PropTypes.object.isRequired,
    medicos: PropTypes.array.isRequired,
    onAddCita: PropTypes.func.isRequired,
    onSetDuracionConsulta: PropTypes.func.isRequired,
    pacientes: PropTypes.array.isRequired,
    timeZone: PropTypes.string.isRequired,
  };

  state = {
    alertStyle: null,
    contrasena: null,
    fecha: null,
    hora: null,
    isConsulta: true,
    observaciones: null,
    paciente: {},
    showModalLogin: false,
    telefono: null,
    tipoPaciente: null,
    usuario: null,
  };

  clearFormCitas = () => {
    document.getElementById('formCitas').reset();
  };

  getInformacionPaciente = paciente => {
    const {citas} = this.props;
    const recurrenciaPaciente = _.filter(citas, {id: paciente.id});

    this.setState({
      alertStyle: recurrenciaPaciente.length !== 0 ? 'success' : 'danger',
      paciente,
      telefono: paciente.telefono,
      tipoPaciente:
        recurrenciaPaciente.length !== 0 ? 'Recurrente' : 'Primera vez',
    });
  };

  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value,
    });
  };

  handleCloseModal = () => {
    this.setState({
      showModalLogin: false,
    });
  };

  handleShowModal = () => {
    this.setState({
      showModalLogin: true,
    });
  };

  handleValidateForm = event => {
    event.preventDefault();

    const {fecha, hora, paciente} = this.state;
    const {medico, timeZone, citas} = this.props;

    if (fecha !== null && hora !== null) {
      const currentDateTime = moment.tz(moment().toDate(), timeZone);
      const fechaHora = moment(
        moment(fecha).format('YYYY-MM-DD') + ' ' + moment(hora).format('HH:mm'),
      );

      if (moment(fechaHora.format()).isAfter(currentDateTime.format())) {
        if (paciente !== null && medico !== null && _.has(paciente, 'id')) {
          const citaPrevia = _.filter(citas, {
            start: new Date(moment.tz(fechaHora, timeZone)),
          });

          if (citaPrevia.length === 0) {
            this.handleShowModal();
          } else {
            toastr.warning(
              'Está intentando agendar una cita en una fecha y hora ' +
                'que ya ha sido asignada. Por favor, revise nuevamente.',
            );
          }
        } else {
          toastr.error(
            'Debe completar la información del formulario antes de ' +
              'intentar agendar una cita. Por favor, revise nuevamente.',
          );
        }
      } else {
        toastr.warning(
          'Está intentando agendar una cita en una fecha u hora ' +
            'anterior a la actual. Por favor, revise nuevamente.',
        );
      }
    }
  };

  onLogin = () => {
    const {usuario} = this.state;

    if (usuario) {
      const {
        contrasena,
        fecha,
        hora,
        isConsulta,
        observaciones,
        paciente,
      } = this.state;
      const {duracionConsulta, medico} = this.props;
      const consulta = isConsulta ? 1 : 0;
      const paciente_id = paciente.id;
      const medico_id = medico.id;
      const passport = {contrasena, usuario};
      const cita = {
        consulta,
        fecha: moment(fecha).format('YYYY-MM-DD'),
        hora: moment(hora).format('HH:mm'),
        observaciones,
        medico_id,
        paciente_id,
        duracion: duracionConsulta,
      };

      this.handleCloseModal();
      this.props.onAddCita(passport, cita);
    }
  };

  render() {
    const {alertStyle, showModalLogin, telefono, tipoPaciente} = this.state;
    const {duracionConsulta} = this.props;

    return (
      <React.Fragment>
        <ModalLogin
          handleChange={this.handleChange}
          handleCloseModal={this.handleCloseModal}
          onLogin={this.onLogin}
          show={showModalLogin}
        />
        <form id="formCitas" onSubmit={this.handleValidateForm}>
          <Row>
            <Col xs={12}>
              <FormGroup>
                <ControlLabel>Paciente:</ControlLabel>
                <Typeahead
                  labelKey={option =>
                    `${option.id}, ${option.nombre}, ${option.domicilio}`
                  }
                  onChange={paciente => {
                    const selectedPaciente = Object.assign({}, paciente[0]);
                    this.getInformacionPaciente(selectedPaciente);
                  }}
                  options={this.props.pacientes}
                  placeholder="Nombre del paciente."
                  ref={ref => (this._typeahead = ref)}
                  renderMenuItemChildren={option => (
                    <div>
                      {option.nombre}
                      <div>
                        <small>Domicilio: {option.domicilio}</small>
                      </div>
                    </div>
                  )}
                />
                <ButtonToolbar style={{marginTop: '10px'}}>
                  <Button onClick={() => this._typeahead.getInstance().clear()}>
                    Limpiar
                  </Button>
                </ButtonToolbar>
              </FormGroup>
            </Col>
          </Row>
          <Row>
            <Col xs={7}>
              <FormGroup>
                <ControlLabel>Fecha:</ControlLabel>
                <DateTimePicker
                  onChange={fecha => this.setState({fecha})}
                  time={false}
                  placeholder="Seleccione una fecha."
                />
              </FormGroup>
            </Col>
            <Col xs={5}>
              <FormGroup>
                <ControlLabel>Hora:</ControlLabel>
                <DateTimePicker
                  onChange={hora => this.setState({hora})}
                  max={new Date(2126, 0, 1, 21, 1)}
                  min={moment()
                    .hour(8)
                    .minute(0)
                    .toDate()}
                  date={false}
                  step={5}
                  placeholder="Seleccione una hora."
                />
              </FormGroup>
            </Col>
          </Row>
          <Row>
            <Col xs={12}>
              <Well>
                <h3>Información del paciente</h3>
                <Row>
                  <Col xs={6}>
                    <h4>Teléfono:</h4>
                    <p>{telefono}</p>
                  </Col>
                  <Col xs={6}>
                    <h4>Tipo de paciente:</h4>
                    <Alert bsStyle={alertStyle}>{tipoPaciente}</Alert>
                  </Col>
                </Row>
              </Well>
            </Col>
          </Row>
          <Row>
            <Col xs={4} xsOffset={2}>
              <FormGroup>
                <ControlLabel>Tipo de cita:</ControlLabel>
                <Radio
                  name="radioGroup"
                  onClick={() => this.setState({isConsulta: true})}
                  defaultChecked>
                  Consulta
                </Radio>
                <Radio
                  name="radioGroup"
                  onClick={() => this.setState({isConsulta: false})}>
                  Revisión
                </Radio>
              </FormGroup>
            </Col>
            <Col xs={4}>
              <FormGroup>
                <ControlLabel>Duración cita (en minutos):</ControlLabel>
                <InputNumber
                  max={780}
                  min={5}
                  onChange={value => this.props.onSetDuracionConsulta(value)}
                  step={5}
                  value={duracionConsulta}
                />
              </FormGroup>
            </Col>
          </Row>
          <Row>
            <Col xs={12}>
              <FormGroup controlId="observaciones">
                <ControlLabel>Observaciones:</ControlLabel>
                <FormControl
                  componentClass="textarea"
                  onChange={this.handleChange}
                  placeholder="Introduzca alguna observación o deje una nota si es necesario."
                />
              </FormGroup>
            </Col>
          </Row>
          <Row>
            <Col xs={3} xsOffset={3}>
              <Button onClick={this.clearFormCitas}>Limpiar</Button>
            </Col>
            <Col xs={3}>
              <Button type="submit">Agendar</Button>
            </Col>
          </Row>
        </form>
      </React.Fragment>
    );
  }
}

class ModalLogin extends Component {
  static propTypes = {
    show: PropTypes.bool.isRequired,
    handleChange: PropTypes.func.isRequired,
    handleCloseModal: PropTypes.func.isRequired,
    onLogin: PropTypes.func.isRequired,
  };

  render() {
    return (
      <React.Fragment>
        <Modal onHide={this.props.handleCloseModal} show={this.props.show}>
          <Modal.Header closeButton>
            <Modal.Title>Introduzca su usuario y contraseña</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <FormGroup>
              <ControlLabel>Usuario:</ControlLabel>
              <FormControl
                id="usuario"
                onChange={this.props.handleChange}
                type="text"
                placeholder="Usuario"
              />
              <br />
              <ControlLabel>Contraseña:</ControlLabel>
              <FormControl
                id="contrasena"
                onChange={this.props.handleChange}
                type="password"
                placeholder="Contraseña"
              />
            </FormGroup>
          </Modal.Body>
          <Modal.Footer>
            <Button bsStyle="primary" onClick={this.props.onLogin}>
              Aceptar
            </Button>
          </Modal.Footer>
        </Modal>
      </React.Fragment>
    );
  }
}
