/*
 * File: Inicio.jsx
 *
 * Created: 27 may 2018 23:54:34
 * Last Modified: 25 jul 2018 08:27:43
 *
 * Copyright (c) 2018, Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 * Author: Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 */

import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import {Grid, Row, Col} from 'react-bootstrap';
import {BeatLoader} from 'react-spinners';
import moment from 'moment-timezone';
import toastr from 'toastr';
import axios from 'axios';

export default class Inicio extends Component {
  static propTypes = {
    beatLoaderColor: PropTypes.string.isRequired,
    beatLoaderSize: PropTypes.number.isRequired,
    expandRowBgColor: PropTypes.string.isRequired,
    language: PropTypes.string.isRequired,
    selectRowBgColor: PropTypes.string.isRequired,
    serverURL: PropTypes.string.isRequired,
    timeZone: PropTypes.string.isRequired,
  };

  state = {
    citas: [],
    error: null,
    loading: true,
    medicos: [],
    updateSuccessful: null,
  };

  onGetCitasDelDia = () => {
    const {serverURL, timeZone} = this.props;
    const currentDate = moment.tz(Date.now(), timeZone).format('YYYY-MM-DD');

    axios
      .get(`${serverURL}/agendas/${currentDate}`)
      .then(response => {
        this.setState({
          citas: response.data,
          error: false,
          loading: false,
        });
      })
      .catch(error => {
        this.setState({
          error,
        });
      });
  };

  onGetMedicos = () => {
    const {serverURL} = this.props;

    axios
      .get(`${serverURL}/medicos`)
      .then(response => {
        this.setState({
          medicos: response.data,
        });
        this.onGetCitasDelDia();
      })
      .catch(error => {
        this.setState({
          error,
        });
      });
  };

  componentDidMount() {
    this.onGetDataTable();
  }

  onGetDataTable() {
    this.onGetMedicos();
  }

  render() {
    const {error, loading, updateSuccessful} = this.state;
    const {
      beatLoaderColor,
      beatLoaderSize,
      expandRowBgColor,
      language,
      selectRowBgColor,
      timeZone,
    } = this.props;
    const currentDate = moment
      .tz(Date.now(), timeZone)
      .format('dddd DD MMMM YYYY');

    moment.locale(language);

    if (error) {
      toastr.error(error.message);
    } else if (updateSuccessful) {
      toastr.success('La transacción se realizó exitosamente.');
    }

    return (
      <React.Fragment>
        <Grid>
          <Row>
            <Col xs={2} xsOffset={5}>
              <BeatLoader
                color={beatLoaderColor}
                loading={loading}
                size={beatLoaderSize}
              />
            </Col>
          </Row>
          <Row>
            <Col xs={12}>
              <h1>
                Citas del día{' '}
                <span className="highlight-title">{currentDate}</span>
              </h1>
            </Col>
          </Row>
          <Row>
            <Col xs={12}>
              <MedicosTable
                expandRowBgColor={expandRowBgColor}
                selectRowBgColor={selectRowBgColor}
                {...this.state}
              />
            </Col>
          </Row>
        </Grid>
      </React.Fragment>
    );
  }
}

class MedicosTable extends Component {
  static propTypes = {
    citas: PropTypes.array.isRequired,
    expandRowBgColor: PropTypes.string.isRequired,
    medicos: PropTypes.array.isRequired,
    selectRowBgColor: PropTypes.string.isRequired,
  };

  tableOptions = {
    clearSearch: true,
    clearSearchBtn: this.createCustomClearButton,
    expandRowBgColor: this.props.expandRowBgColor,
    hidePageListOnlyOnePage: true,
    noDataText: 'No existe información para mostrar.',
  };

  isExpandableRow = row => {
    const {citas} = this.props;
    const cita = citas.filter(cita => cita.medico_id === row.id);

    return cita.length > 0 ? true : false;
  };

  expandComponent = row => {
    const {citas, selectRowBgColor} = this.props;
    const cita = citas.filter(cita => cita.medico_id === row.id);

    return (
      <ExpandablePacientesTable
        cita={cita}
        selectRowBgColor={selectRowBgColor}
      />
    );
  };

  createCustomClearButton(onClick) {
    return (
      <button className="btn btn-default" onClick={onClick}>
        Limpiar
      </button>
    );
  }

  render() {
    return (
      <BootstrapTable
        data={this.props.medicos}
        expandableRow={this.isExpandableRow}
        expandComponent={this.expandComponent}
        expandColumnOptions={{expandColumnVisible: true}}
        hover={true}
        options={this.tableOptions}
        pagination={true}
        search={true}
        searchPlaceholder="Buscar"
        striped={true}>
        <TableHeaderColumn
          dataField="id"
          dataSort={true}
          hidden={true}
          isKey={true}
          width="5%">
          Id
        </TableHeaderColumn>
        <TableHeaderColumn
          dataField="nombre"
          dataSort={true}
          searchable={true}
          width="25%">
          Nombre
        </TableHeaderColumn>
        <TableHeaderColumn dataField="telefono" width="15%">
          Teléfono
        </TableHeaderColumn>
        <TableHeaderColumn
          dataField="horario"
          tdStyle={{whiteSpace: 'pre-line'}}>
          Horario
        </TableHeaderColumn>
        <TableHeaderColumn
          dataField="procedimientos"
          tdStyle={{whiteSpace: 'pre-line'}}>
          Procedimientos
        </TableHeaderColumn>
      </BootstrapTable>
    );
  }
}

class ExpandablePacientesTable extends Component {
  static propTypes = {
    cita: PropTypes.array.isRequired,
    selectRowBgColor: PropTypes.string.isRequired,
  };

  selectRowProp = {
    bgColor: this.props.selectRowBgColor,
    mode: 'radio',
  };

  tableOptions = {
    noDataText: 'No existe información para mostrar.',
  };

  citaFormatter(cell) {
    return cell === 1 ? 'Consulta' : 'Revisión';
  }

  fechaFormatter(cell) {
    return cell.split('T')[0];
  }

  render() {
    return (
      <BootstrapTable
        data={this.props.cita}
        hover={true}
        options={this.tableOptions}
        selectRow={this.selectRowProp}
        striped={true}>
        <TableHeaderColumn dataField="id" hidden={true} isKey={true} width="5%">
          Id
        </TableHeaderColumn>
        <TableHeaderColumn
          dataField="nombre"
          editable={false}
          tdStyle={{whiteSpace: 'pre-line'}}>
          Nombre
        </TableHeaderColumn>
        <TableHeaderColumn
          dataField="domicilio"
          editable={false}
          tdStyle={{whiteSpace: 'pre-line'}}>
          Domicilio
        </TableHeaderColumn>
        <TableHeaderColumn dataField="telefono" editable={false} width="10%">
          Teléfono
        </TableHeaderColumn>
        <TableHeaderColumn
          dataField="fecha"
          dataFormat={this.fechaFormatter}
          editable={false}
          width="8%">
          Fecha
        </TableHeaderColumn>
        <TableHeaderColumn dataField="hora" editable={false} width="8%">
          Hora
        </TableHeaderColumn>
        <TableHeaderColumn
          dataField="consulta"
          dataFormat={this.citaFormatter}
          editable={false}
          width="8%">
          Cita
        </TableHeaderColumn>
        <TableHeaderColumn
          dataField="observaciones"
          editable={false}
          tdStyle={{whiteSpace: 'pre-line'}}>
          Observaciones
        </TableHeaderColumn>
      </BootstrapTable>
    );
  }
}
