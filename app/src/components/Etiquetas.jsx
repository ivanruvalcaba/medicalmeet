/*
 * File: Etiquetas.jsx
 *
 * Created: 07 ago 2018 15:20:50
 * Last Modified: 07 ago 2018 19:08:40
 *
 * Copyright (c) 2018, Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 * Author: Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/.
 */

import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {
  Grid,
  Row,
  Col,
  FormGroup,
  ControlLabel,
  Button,
  ButtonToolbar,
} from 'react-bootstrap';
import {BeatLoader} from 'react-spinners';
import {Typeahead} from 'react-bootstrap-typeahead';
import Barcode from 'react-barcode';
import ReactToPrint from 'react-to-print';
import toastr from 'toastr';
import axios from 'axios';

export default class Pacientes extends Component {
  static propTypes = {
    beatLoaderColor: PropTypes.string.isRequired,
    beatLoaderSize: PropTypes.number.isRequired,
    serverURL: PropTypes.string.isRequired,
  };

  state = {
    codigoBarras: 'MedicalMeet',
    error: null,
    loading: true,
    pacientes: [],
  };

  onGetPacientes = () => {
    const {serverURL} = this.props;

    axios
      .get(`${serverURL}/pacientes`)
      .then(response => {
        this.setState({
          error: false,
          loading: false,
          pacientes: response.data,
        });
      })
      .catch(error => {
        this.setState({
          error,
        });
      });
  };

  onGenerarCodigoBarras = paciente => {
    this.setState({
      codigoBarras: 'ceh-' + String(paciente.id).padStart(10, '0'),
    });
  };

  componentDidMount() {
    this.onGetPacientes();
  }

  render() {
    const {error, loading, pacientes, codigoBarras} = this.state;
    const {beatLoaderColor, beatLoaderSize} = this.props;

    if (error) {
      toastr.error(error.message);
    }

    return (
      <React.Fragment>
        <Grid>
          <Row>
            <Col xs={2} xsOffset={5}>
              <BeatLoader
                color={beatLoaderColor}
                loading={loading}
                size={beatLoaderSize}
              />
            </Col>
          </Row>
          <Row>
            <Col xs={12}>
              <h1>Etiquetas</h1>
            </Col>
          </Row>
          <Row>
            <Col xs={4} xsOffset={4}>
              <FormGroup>
                <ControlLabel>Paciente:</ControlLabel>
                <Typeahead
                  labelKey={option =>
                    `${option.id}, ${option.nombre}, ${option.domicilio}`
                  }
                  onChange={paciente => {
                    const selectedPaciente = Object.assign({}, paciente[0]);
                    this.onGenerarCodigoBarras(selectedPaciente);
                  }}
                  options={pacientes}
                  placeholder="Nombre del paciente."
                  ref={ref => (this._typeahead = ref)}
                  renderMenuItemChildren={option => (
                    <div>
                      {option.nombre}
                      <div>
                        <small>Domicilio: {option.domicilio}</small>
                      </div>
                    </div>
                  )}
                />
                <ButtonToolbar style={{marginTop: '10px'}}>
                  <Button onClick={() => this._typeahead.getInstance().clear()}>
                    Limpiar
                  </Button>
                </ButtonToolbar>
              </FormGroup>
            </Col>
          </Row>
          <Row>
            <Col xs={4} xsOffset={4}>
              <center>
                <Barcode
                  backgroud="#f5f5f5"
                  format="CODE128"
                  height={60}
                  lineColor="#333333"
                  ref={el => (this.componentRef = el)}
                  textAlign="center"
                  value={codigoBarras}
                  width={2}
                />
              </center>
            </Col>
          </Row>
          <br />
          <Row>
            <Col xs={2} xsOffset={5}>
              <ReactToPrint
                trigger={() => (
                  <Button bsSize="large" bsStyle="primary">
                    Imprimir etiqueta
                  </Button>
                )}
                content={() => this.componentRef}
              />
            </Col>
          </Row>
        </Grid>
      </React.Fragment>
    );
  }
}
