/*
 * File: Usuarios.jsx
 *
 * Created: 04 jul 2018 12:29:14
 * Last Modified: 04 jul 2018 13:45:07
 *
 * Copyright (c) 2018, Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 * Author: Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/.
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { BootstrapTable, TableHeaderColumn, InsertModalHeader,
  InsertModalFooter } from 'react-bootstrap-table';
import { Grid, Row, Col } from 'react-bootstrap';
import { BeatLoader } from 'react-spinners';
import toastr from 'toastr';
import axios from 'axios';
import _ from 'lodash';

export default class Usuarios extends Component {
  static propTypes = {
    beatLoaderColor: PropTypes.string.isRequired,
    beatLoaderSize: PropTypes.number.isRequired,
    selectRowBgColor: PropTypes.string.isRequired,
    serverURL: PropTypes.string.isRequired
  };

  state = {
    error: null,
    loading: true,
    updateSuccessful: null,
    usuarios: []
  }

  onAddRow = (row) => {
    const { serverURL } = this.props;

    axios.post(`${serverURL}/usuarios`, row)
      .then(response => {
        const { usuarios } = this.state;

        usuarios.push(response.data);

        this.setState({
          usuarios,
          updateSuccessful: true
        });
      })
      .catch(error => {
        this.setState({
          error,
          updateSuccessful: false
        });
      });
  }

  onCellEdit = (row, fieldName, value) => {
    const { serverURL } = this.props;
    const register = Object.assign({}, row);

    register[fieldName] = value;

    axios.put(`${serverURL}/usuarios/${row.id}`, register)
      .then(response => {
        if (response.status === 204) {
          const { usuarios } = this.state;
          const usuario = _.find(usuarios, { 'id': row.id });

          if (usuario !== undefined) {
            const index = _.findIndex(usuarios, { 'id': row.id });

            usuario[fieldName] = value;
            usuarios[index] = usuario;

            this.setState({
              usuarios,
              updateSuccessful: true
            });
          }
        }
      })
      .catch(error => {
        this.setState({
          updateSuccessful: false,
          error: error
        });
      });
  }

  onDeleteRow = (id) => {
    const { serverURL } = this.props;

    axios.delete(`${serverURL}/usuarios/${id}`)
      .then(response => {
        if (response.status === 204) {
          let { usuarios } = this.state;

          usuarios = usuarios.filter((usuario) => {
            return usuario.id !== id[0];
          });

          this.setState({
            usuarios,
            updateSuccessful: true
          });
        }
      })
      .catch(error => {
        this.setState({
          error,
          updateSuccessful: false
        });
      });
  }

  onGetUsuarios = () => {
    const { serverURL } = this.props;

    axios.get(`${serverURL}/usuarios`)
      .then(response => {
        this.setState({
          error: false,
          loading: false,
          usuarios: response.data
        });
      })
      .catch(error => {
        this.setState({
          error
        });
      });
  }

  componentDidMount() {
    this.onGetUsuarios();
  }

  render() {
    const { error, loading, updateSuccessful } = this.state;
    const { beatLoaderColor, beatLoaderSize, selectRowBgColor } = this.props;

    if (error) {
      toastr.error(error.message);
    }
    else if (updateSuccessful) {
      toastr.success('La transacción se realizó exitosamente.');
    }

    return (
      <React.Fragment>
        <Grid>
          <Row>
            <Col xs={ 2 } xsOffset={ 5 }>
              <BeatLoader color={ beatLoaderColor } loading={ loading }
                size={ beatLoaderSize } />
            </Col>
          </Row>
          <Row>
            <Col xs={ 12 }>
              <h1>Usuarios</h1>
            </Col>
          </Row>
          <Row>
            <Col xs={ 12 }>
              <UsuariosTable
                onAddRow={ this.onAddRow }
                onCellEdit={ this.onCellEdit }
                onDeleteRow={ this.onDeleteRow }
                selectRowBgColor={ selectRowBgColor }
                { ...this.state }
              />
            </Col>
          </Row>
        </Grid>
      </React.Fragment>
    );
  }
}

class UsuariosTable extends Component {
  static propTypes = {
    onAddRow: PropTypes.func.isRequired,
    onCellEdit: PropTypes.func.isRequired,
    onDeleteRow: PropTypes.func.isRequired,
    selectRowBgColor: PropTypes.string.isRequired,
    usuarios: PropTypes.array.isRequired
  }

  state = {
    tipoUsuario: [ { id: 1, value: 'Administrador' }, { id: 2, value: 'Usuario' },
      { id: 3, value: 'Médico' } ]
  }

  cellEditProp = {
    blurToSave: true,
    mode: 'dbclick'
  };

  selectRowProp = {
    bgColor: this.props.selectRowBgColor,
    mode: 'radio'
  };

  tableOptions = {
    clearSearch: true,
    clearSearchBtn: this.createCustomClearButton,
    deleteText: 'Eliminar',
    exportCSVText: 'Exportar a Excel',
    handleConfirmDeleteRow: this.createCustomConfirmDeleteRow,
    hidePageListOnlyOnePage: true,
    insertModalHeader: this.createCustomModalHeader,
    insertModalFooter: this.createCustomModalFooter,
    insertText: 'Nuevo',
    noDataText: 'No existe información para mostrar.',
    onAddRow: this.props.onAddRow,
    onCellEdit: this.props.onCellEdit,
    onDeleteRow: this.props.onDeleteRow
  };

  tipoUsuarioFormatter = (cell) => {
    const { tipoUsuario } = this.state;
    const value = this.handleGainValue(tipoUsuario,
      'value', cell);

    return value;
  }

  tipoUsuarioCVSFormatter = (cell) => {
    const { tipoUsuario } = this.state;
    const value = this.handleGainValue(tipoUsuario,
      'value', cell);

    return value;
  }

  createCustomModalHeader() {
    return (
      <InsertModalHeader title='Agregar usuario' />
    );
  }

  createCustomModalFooter() {
    return (
      <InsertModalFooter saveBtnText='Guardar' closeBtnText='Cerrar' />
    );
  }

  createCustomClearButton(onClick) {
    return (
      <button className='btn btn-default' onClick={ onClick } >
        Limpiar
      </button>
    );
  }

  createCustomConfirmDeleteRow(next) {
    if (confirm('¿Desea confirmar la eliminación del registro?')) {
      next();
    }
  }

  contrasenaFormatter() {
    return '';
  }

  telefonoCVSFormatter(cell) {
    return cell === null ? '' : cell;
  }

  handleDataValidator(value) {
    const response = { isValid: true, notification: { type: 'success', msg: '', title: '' } };

    if (!value) {
      response.isValid = false;
      response.notification.type = 'error';
      response.notification.msg = 'Este campo debe ser completado.';
      response.notification.title = 'Información requerida.';
    }

    return response;
  }

  handleGainValue(collection, key, value) {
    const object = _.find(collection, { id: value });
    let returnedValue = '';

    if (_.has(object, key))
      returnedValue = object[key];

    return returnedValue;
  }

  render() {
    return (
      <BootstrapTable
        cellEdit={ this.cellEditProp }
        data={ this.props.usuarios }
        deleteRow={ true }
        exportCSV={ true }
        hover={ true }
        insertRow={ true }
        options= { this.tableOptions }
        pagination={ true }
        search={ true }
        searchPlaceholder='Buscar'
        selectRow={ this.selectRowProp }
        striped={ true }
      >
        <TableHeaderColumn
          autoValue={ true }
          dataField="id"
          dataSort={ true }
          hidden={ true }
          isKey={ true }
          width='5%'
        >
          Id
        </TableHeaderColumn>
        <TableHeaderColumn
          dataField="nombre"
          dataSort={ true }
          editable={ { validator: this.handleDataValidator } }
          tdStyle={ { whiteSpace: 'pre-line' } }
          searchable={ true }
        >
          Nombre
        </TableHeaderColumn>
        <TableHeaderColumn
          csvFormat={ this.telefonoCVSFormatter }
          dataField="telefono"
          width='15%'
        >
          Teléfono
        </TableHeaderColumn>
        <TableHeaderColumn
          dataField="usuario"
          editable={ { validator: this.handleDataValidator } }
          width='20%'
        >
          Usuario
        </TableHeaderColumn>
        <TableHeaderColumn
          dataField="contrasena"
          dataFormat={ this.contrasenaFormatter }
          editable={ { type: 'password', validator: this.handleDataValidator } }
          width='20%'
        >
          Contraseña
        </TableHeaderColumn>
        <TableHeaderColumn
          csvFormat={ this.tipoUsuarioCVSFormatter }
          dataField="tipo_usuario"
          dataFormat={ this.tipoUsuarioFormatter }
          editable={ { type: 'select', options: { values: this.state.tipoUsuario, textKey: 'value', valueKey: 'id' } } }
          width='20%'
        >
          Tipo usuario
        </TableHeaderColumn>
      </BootstrapTable>
    );
  }
}
