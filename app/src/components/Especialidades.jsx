/*
 * File: Especialidades.jsx
 *
 * Created: 21 may 2018 13:50:15
 * Last Modified: 03 jul 2018 14:33:43
 *
 * Copyright (c) 2018, Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 * Author: Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/.
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { BootstrapTable, TableHeaderColumn, InsertModalHeader, InsertModalFooter } from 'react-bootstrap-table';
import { Grid, Row, Col } from 'react-bootstrap';
import { BeatLoader } from 'react-spinners';
import toastr from 'toastr';
import axios from 'axios';
import _ from 'lodash';

export default class Especialidades extends Component {
  static propTypes = {
    beatLoaderColor: PropTypes.string.isRequired,
    beatLoaderSize: PropTypes.number.isRequired,
    expandRowBgColor: PropTypes.string.isRequired,
    selectRowBgColor: PropTypes.string.isRequired,
    serverURL: PropTypes.string.isRequired
  }

  state = {
    error: null,
    especialidades: [],
    loading: true,
    medicos: [],
    updateSuccessful: null
  };

  onAddRow = (row) => {
    const { serverURL } = this.props;

    axios.post(`${serverURL}/especialidades`, row)
      .then(response => {
        const { especialidades } = this.state;

        especialidades.push(response.data);

        this.setState({
          especialidades,
          updateSuccessful: true
        });
      })
      .catch(error => {
        this.setState({
          error,
          updateSuccessful: false
        });
      });
  }

  onCellEdit = (row, fieldName, value) => {
    const { serverURL } = this.props;
    const register = Object.assign({}, row);

    register[fieldName] = value;

    axios.put(`${serverURL}/especialidades/${row.id}`, register)
      .then(response => {
        if (response.status === 204) {
          const { especialidades } = this.state;
          const especialidad = _.find(especialidades, { 'id': row.id });

          if (especialidad !== undefined) {
            const index = _.findIndex(especialidades, { 'id': row.id });

            especialidad[fieldName] = value;
            especialidades[index] = especialidad;

            this.setState({
              especialidades,
              updateSuccessful: true
            });
          }
        }
      })
      .catch(error => {
        this.setState({
          error,
          updateSuccessful: false
        });
      });
  }

  onDeleteRow = (id) => {
    const { serverURL } = this.props;

    axios.delete(`${serverURL}/especialidades/${id}`)
      .then(response => {
        if (response.status === 204) {
          let { especialidades } = this.state;

          especialidades = especialidades.filter((especialidad) => {
            return especialidad.id !== id[0];
          });

          this.setState({
            especialidades,
            updateSuccessful: true
          });
        }
      })
      .catch(error => {
        this.setState({
          error,
          updateSuccessful: false
        });
      });
  }

  onGetEspecialidades = () => {
    const { serverURL } = this.props;

    axios.get(`${serverURL}/especialidades`)
      .then(response => {
        this.setState({
          especialidades: response.data
        });
        this.onGetMedicos();
      })
      .catch(error => {
        this.setState({
          error
        });
      });
  }

  onGetMedicos = () => {
    const { serverURL } = this.props;

    axios.get(`${serverURL}/medicos`)
      .then(response => {
        const medicos = _.map(response.data,
          medico => _.omit(medico, [ 'observaciones', 'created_at', 'updated_at' ]));

        this.setState({
          error: false,
          loading: false,
          medicos
        });
      })
      .catch(error => {
        this.setState({
          error
        });
      });
  }

  componentDidMount() {
    this.onGetDataTable();
  }

  onGetDataTable() {
    this.onGetEspecialidades();
  }

  render() {
    const { error, loading, updateSuccessful } = this.state;
    const { beatLoaderColor, beatLoaderSize, expandRowBgColor,
      selectRowBgColor } = this.props;

    if (error) {
      toastr.error(error.message);
    }
    else if (updateSuccessful) {
      toastr.success('La transacción se realizó exitosamente.');
    }

    return (
      <React.Fragment>
        <Grid>
          <Row>
            <Col xs={ 2 } xsOffset={ 5 }>
              <BeatLoader color={ beatLoaderColor } loading={ loading }
                size={ beatLoaderSize } />
            </Col>
          </Row>
          <Row>
            <Col xs={ 12 }>
              <h1>Especialidades</h1>
            </Col>
          </Row>
          <Row>
            <Col xs={ 12 }>
              <EspecialidadesTable
                expandRowBgColor={ expandRowBgColor }
                onAddRow={ this.onAddRow }
                onCellEdit={ this.onCellEdit }
                onDeleteRow={ this.onDeleteRow }
                selectRowBgColor={ selectRowBgColor }
                { ...this.state }
              />
            </Col>
          </Row>
        </Grid>
      </React.Fragment>
    );
  }
}

class EspecialidadesTable extends Component {
  static propTypes = {
    especialidades: PropTypes.array.isRequired,
    expandRowBgColor: PropTypes.string.isRequired,
    medicos: PropTypes.array.isRequired,
    onAddRow: PropTypes.func.isRequired,
    onCellEdit: PropTypes.func.isRequired,
    onDeleteRow: PropTypes.func.isRequired,
    selectRowBgColor: PropTypes.string.isRequired
  };

  cellEditProp = {
    blurToSave: true,
    mode: 'dbclick'
  };

  selectRowProp = {
    bgColor: this.props.selectRowBgColor,
    clickToExpand: true,
    mode: 'radio'
  };

  tableOptions = {
    clearSearch: true,
    clearSearchBtn: this.createCustomClearButton,
    deleteText: 'Eliminar',
    expandRowBgColor: this.props.expandRowBgColor,
    exportCSVText: 'Exportar a Excel',
    handleConfirmDeleteRow: this.createCustomConfirmDeleteRow,
    hidePageListOnlyOnePage: true,
    insertModalHeader: this.createCustomModalHeader,
    insertModalFooter: this.createCustomModalFooter,
    insertText: 'Nuevo',
    onAddRow: this.props.onAddRow,
    onCellEdit: this.props.onCellEdit,
    onDeleteRow: this.props.onDeleteRow,
    noDataText: 'No existe información para mostrar.'
  };

  expandComponent = (row) => {
    const { medicos, selectRowBgColor } = this.props;
    const medicosEspecialidad = medicos.filter(
      medico => medico.especialidad_id === row.id);

    return (
      <ExpandableMedicosTable medicos={ medicosEspecialidad }
        selectRowBgColor={ selectRowBgColor } />
    );
  }

  isExpandableRow = (row) => {
    const { medicos } = this.props;
    const medicosEspecialidad = medicos.filter(
      medico => medico.especialidad_id === row.id);

    return medicosEspecialidad.length > 0 ? true : false;
  }

  handleDataValidator(value) {
    const response = { isValid: true, notification: { type: 'success', msg: '', title: '' } };

    if (!value) {
      response.isValid = false;
      response.notification.type = 'error';
      response.notification.msg = 'Este campo debe ser completado.';
      response.notification.title = 'Información requerida.';
    }

    return response;
  }

  createCustomModalHeader() {
    return (
      <InsertModalHeader title='Agregar especialidad' />
    );
  }

  createCustomModalFooter() {
    return (
      <InsertModalFooter saveBtnText='Guardar' closeBtnText='Cerrar' />
    );
  }

  createCustomClearButton(onClick) {
    return (
      <button className='btn btn-default' onClick={ onClick } >
        Limpiar
      </button>
    );
  }

  createCustomConfirmDeleteRow(next) {
    if (confirm('¿Desea confirmar la eliminación del registro?')) {
      next();
    }
  }

  render() {
    return (
      <BootstrapTable
        cellEdit={ this.cellEditProp }
        data={ this.props.especialidades }
        deleteRow={ true }
        exportCSV={ true }
        expandColumnOptions={ { expandColumnVisible: true } }
        expandComponent={ this.expandComponent }
        expandableRow={ this.isExpandableRow }
        hover={ true }
        insertRow={ true }
        options= { this.tableOptions }
        pagination={ true }
        selectRow={ this.selectRowProp }
        search={ true }
        searchPlaceholder='Buscar'
        striped={ true }
      >
        <TableHeaderColumn
          autoValue={ true }
          dataField="id"
          dataSort={ true }
          hidden={ true }
          isKey={ true }
          width='5%'
        >
          Id
        </TableHeaderColumn>
        <TableHeaderColumn
          dataField="especialidad"
          dataSort={ true }
          editable={ { validator: this.handleDataValidator } }
          searchable={ true }
          width='25%'
        >
          Especialidad
        </TableHeaderColumn>
        <TableHeaderColumn
          dataField="descripcion"
          editable={ { type: 'textarea', validator: this.handleDataValidator } }
          searchable={ true }
          tdStyle={ { whiteSpace: 'pre-line' } }
        >
          Descripción
        </TableHeaderColumn>
      </BootstrapTable>
    );
  }
}


class ExpandableMedicosTable extends Component {
  static propTypes =  {
    medicos: PropTypes.array.isRequired,
    selectRowBgColor: PropTypes.string.isRequired
  };

  selectRowProp = {
    bgColor: this.props.selectRowBgColor,
    mode: 'radio'
  };

  tableOptions = {
    noDataText: 'No existe información para mostrar.'
  };

  render() {
    return (
      <BootstrapTable
        data={ this.props.medicos }
        hover={ true }
        options={ this.tableOptions }
        selectRow={ this.selectRowProp }
        striped={ true }
      >
        <TableHeaderColumn
          dataField="id"
          hidden={ true }
          isKey={ true }
          width='5%'
        >
          Id
        </TableHeaderColumn>
        <TableHeaderColumn
          dataField="nombre"
          width='20%'
        >
          Nombre
        </TableHeaderColumn>
        <TableHeaderColumn
          dataField="telefono"
          width='10%'
        >
          Teléfono
        </TableHeaderColumn>
        <TableHeaderColumn
          dataField="email"
          width='20%'
        >
          Email
        </TableHeaderColumn>
        <TableHeaderColumn
          dataField="horario"
          tdStyle={ { whiteSpace: 'pre-line' } }
        >
          Horario
        </TableHeaderColumn>
      </BootstrapTable>
    );
  }
}
